import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';


@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly repo: Repository<User>,
  ) { }

  async createAndupdate(id: string, form): Promise<User> {
    console.log(form)
    if (id) {//update
      const user = await this.findOne(id);
      return this.repo.save({
        ...user,
        ...form
      });
    } else {//create     
      return this.repo.save({
        ...form
      });
    }
  }

  async findAll(): Promise<User[]> {
    return this.repo.find();
  }

  async findOne(id: string): Promise<User> {
    return this.repo.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.repo.delete(id);
  }

  async login(dados): Promise<any> {
    console.log("ok");
    console.log(JSON.stringify(dados));
    let pessoa = await this.repo.findOne({email: dados.name})
    console.log(JSON.stringify(pessoa));
    
    
    if (pessoa && dados.name == pessoa.email && dados.pass == pessoa.password) {
      console.log('boa');

      return {retorno: "ok", user: pessoa }
    } else {
      console.log("error");
      return {retorno: "error", user: 0 }
    }
  }
}
