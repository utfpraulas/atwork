import { Body, Controller, Post, Get, Param } from "@nestjs/common";
import { UsersService } from "./user.service";

@Controller('user')
export class UserController {
  constructor(
    private readonly service: UsersService,
  ) { }

  @Post('login')
  async login(@Body() dados): Promise<string> {
    //chama a validaçao
    const token = await this.service.login(dados.body)
    console.log(`token ${JSON.stringify(token)}`);
    
    return token;
  }
  @Post('profile/:id')
  async profile(@Param() id): Promise<any> {
    //chama a validaçao
    const perfil = await this.service.findOne(id)
    return perfil
  }
  @Get()
  async profiles(): Promise<any> {
    //chama a validaçao
    const perfil = await this.service.findAll()
    return perfil
  }
}