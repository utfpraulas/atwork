import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  department: string;
  
  @Column({nullable: true})
  name: string;

  @Column({ default: true })
  stats: string;

  @Column({ default: true })
  social: string;

  @Column({nullable: true})
  about: string;

  @Column({nullable: true})
  avatar: string;

  @Column({ nullable: true })
  locale: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true })
  password: string;

  @Column({ nullable: true })
  funcao: string;
    
}
