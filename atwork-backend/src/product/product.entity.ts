import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Product {
  @PrimaryGeneratedColumn("uuid")
  id: number;

  @Column()
  type: string;
  
  @Column({nullable: true})
  title: string;
    
  @Column({nullable: true})
  linkLabel: string;
    
  @Column({nullable: true})
  image: string;
    
}
