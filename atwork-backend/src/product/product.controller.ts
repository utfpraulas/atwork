import { Body, Controller, Post, Get } from "@nestjs/common";
import { ProductsService } from "./product.service";

@Controller('product')
export class ProductController {
  constructor(
    private readonly service: ProductsService,
  ) { }
  
  @Get()
  async profiles(): Promise<any> {
    //chama a validaçao
    const perfil = await this.service.findAll()
    return perfil
  }
}