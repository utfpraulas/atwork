import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './product.entity';


@Injectable()
export class  ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly repo: Repository<Product>,
  ) { }

  async createAndupdate(id: string, form): Promise< Product> {
    console.log(form)
    if (id) {//update
      const product = await this.findOne(id);
      return this.repo.save({
        ...product,
        ...form
      });
    } else {//create     
      return this.repo.save({
        ...form
      });
    }
  }

  async findAll(): Promise< Product[]> {
    return this.repo.find();
  }

  async findOne(id: string): Promise<Product> {
    return this.repo.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.repo.delete(id);
  }

  async login(dados): Promise<string>{
    return 'aqui vem o token'
  }
}
