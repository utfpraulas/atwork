import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductController } from './product.controller';
import { Product } from './product.entity';
import { ProductsService } from './product.service';


@Module({
  imports: [TypeOrmModule.forFeature([Product]),],
  providers: [ProductsService],
  controllers: [ProductController],
})
export class ProductsModule {}
