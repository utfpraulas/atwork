import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Validation } from './validation.entity';
import { ValidationsService } from './validation.service';


@Module({
  imports: [TypeOrmModule.forFeature([Validation]),],
  providers: [ValidationsService],
  controllers: [],
})
export class ValidationsModule {}
