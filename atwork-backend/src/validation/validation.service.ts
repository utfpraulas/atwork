import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Validation } from './validation.entity';

//fazer um cron que fica rodando e removendo os tokens antigos nao validos

@Injectable()
export class ValidationsService {
  constructor(
    @InjectRepository(Validation)
    private readonly repo: Repository<Validation>,
  ) { }

  async create(userid): Promise<Validation> {
    //montar o form aqui dentro mesmo id auto gereted
    //pegar data atual do sistema para salvar
    //e relacionar com o user que vem no signo da funcao
    let form
    console.log(form)
    return await this.repo.save({
      ...form
    });
  }

  async findOne(id: string): Promise<Validation> {
    return this.repo.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.repo.delete(id);
  }

  async validation(id): Promise<string> {
    return 'aqui valido a data e o se esta valido, se nao estiver, retorn -1 se tiver retorna ok '
  }
}
