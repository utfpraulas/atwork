import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Ponto {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dia: string;
  
  @Column({nullable: true})
  horario: string;

  @Column()
  motivo: string;
    
  @Column()
  userid: number;
}
