import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PontoController } from './ponto.controller';
import { Ponto } from './ponto.entity';
import { PontosService } from './ponto.service';


@Module({
  imports: [TypeOrmModule.forFeature([Ponto]),],
  providers: [PontosService],
  controllers: [PontoController],
})
export class PontosModule {}
