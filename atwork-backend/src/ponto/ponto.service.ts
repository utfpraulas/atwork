import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Ponto } from './ponto.entity';
	
const moment = require("moment");


@Injectable()
export class PontosService {
  constructor(
    @InjectRepository(Ponto)
    private readonly repo: Repository<Ponto>,
  ) { }

  async createAndupdate(id: number, form): Promise<Ponto> {
    console.log('form: ', JSON.stringify(form));
    console.log(form.dia);
    

    console.log(form)
    if (id == 0) {//update
      const user = await this.findOne(id);
      return this.repo.save({
        ...user,
        ...form
      });
    } else {//create     
      return this.repo.save({
        ...form
      });
    }
  }

  async findAll(): Promise<Ponto[]> {
    return this.repo.find();
  }

  async findOne(id: number): Promise<Ponto> {
    return this.repo.findOne({userid: id})
  }

  async remove(id: string): Promise<void> {
    await this.repo.delete(id);
  }

}
