import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from './config/config.service';
import { UsersModule } from './users/user.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ValidationsModule } from './validation/validation.module';
import { PontosModule } from './ponto/ponto.module';

@Module({
  imports: [TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    UsersModule,
    ValidationsModule,
    PontosModule
  ],
  controllers: [
    AppController,
  ],
  providers: [AppService],
})
export class AppModule {}


