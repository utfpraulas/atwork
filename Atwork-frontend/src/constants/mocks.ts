import dayjs from 'dayjs';
import axios from 'axios';
import * as global from './globals'

import {
  IArticle,
  IArticleOptions,
  IBasket,
  ICategory,
  IExtra,
  ILista,
  ILocation,
  INotification,
  IProduct,
  IUser,
} from './types';

export function userss() {
  console.log(global.userid);
  console.log(`aquiss`);
/*let teste
 teste =  axios.get(`http://192.168.0.101:3001/nota10/user/profile/${global.userid}`)
  if (teste.data) {
    //return valores.data
  }

  let valores =
    {
      id: 1,
      name: 'please',
      department: 'Desenvolvedora de Software',
      stats: { posts: 323, followers: 53200, following: 749000 },
      social: { twitter: 'VICTOR', dribbble: 'VictorR' },
      about:
        'Tempo de Empresa: 2 anos 3 meses e 5 dias.\nSetor: Fábrica de Software',
      avatar:
        'https://cdn-icons-png.flaticon.com/512/64/64572.png',
    }
    return valores*/
    return USERS[0]
}
export function setuserss(valor: any) {
  console.log(global.userid);
  console.log(`aquiss`);
/*let teste
 teste =  axios.get(`http://192.168.0.101:3001/nota10/user/profile/${global.userid}`)
  if (teste.data) {
    //return valores.data
  }

  let valores =
    {
      id: 1,
      name: 'please',
      department: 'Desenvolvedora de Software',
      stats: { posts: 323, followers: 53200, following: 749000 },
      social: { twitter: 'VICTOR', dribbble: 'VictorR' },
      about:
        'Tempo de Empresa: 2 anos 3 meses e 5 dias.\nSetor: Fábrica de Software',
      avatar:
        'https://cdn-icons-png.flaticon.com/512/64/64572.png',
    }
    return valores*/
   USERS[0] =  valor
}

function produtos() {
  axios.get('http://localhost:8080/admin/product')
    .then(response => {
      console.log(response.data);
      //return response.data
    });

  let iProduct = [
    {
      id: 1,
      type: 'horizontal',
      title: 'FeedBacks',
      linkLabel: 'FeedBacks',
      image: 'https://cdn-icons-png.flaticon.com/512/1533/1533913.png',
    },
    {
      id: 2,
      type: 'horizontal',
      title: 'Férias',
      linkLabel: 'Ferias',
      image: 'https://cdn-icons.flaticon.com/png/512/3634/premium/3634817.png?token=exp=1651715375~hmac=6a6772475362dc43fd9748b9964adcbb'
    },
    {
      id: 3,
      type: 'horizontal',
      title: 'Acerto de Ponto',
      linkLabel: 'Ponto',
      image: 'https://cdn-icons-png.flaticon.com/512/2802/2802888.png'
    },
    {
      id: 4,
      type: 'horizontal',
      title: 'Financeiro',
      linkLabel: 'Financeiro',
      image:
        'https://cdn-icons-png.flaticon.com/512/2272/2272758.png'
    }
  ]
  return iProduct
}


/*async*/ function dados()/*: Promise<IUser[]>*/ {
  console.log(global.userid);
  /*let valores
  valores = await axios.get(`http://192.168.0.101:3001/nota10/user/profile/${global.userid}`)
  if (valores.data) {
    return valores.data
  }
  let retornovazio = [{
    id: 0,
    name: 'Name',
    department: 'Departament',
    stats: { posts: 323, followers: 53200, following: 749000 },
    social: { twitter: '', dribbble: '' },
    about:
      'Sem user logado',
    avatar:
      'https://cdn-icons-png.flaticon.com/512/64/64572.png',
  }]
  return retornovazio*/

  let valores = [
    {
      id: 0,
      name: 'teste',
      department: 'Desenvolvedora de Software',
      stats: { posts: 323, followers: 53200, following: 749000 },
      social: { twitter: 'VICTOR', dribbble: 'VictorR' },
      about:
        'Tempo de Empresa: 2 anos 3 meses e 5 dias.\nSetor: Fábrica de Software',
      avatar:
        'https://cdn-icons-png.flaticon.com/512/64/64572.png',
    },
    {
      id: 2,
      name: 'Victor Rafael',
      department: 'Marketing Manager',
      stats: { posts: 323, followers: 53200, following: 749000 },
      social: { twitter: 'VictorR', dribbble: 'VictorR' },
      about:
        'Tempo de Empresa: 2 anos 3 meses e 5 dias.\nSetor: Fábrica de Software',
      avatar:
        'https://cdn-icons-png.flaticon.com/512/64/64572.png',
    },
  ]
  return valores;
}


// users
export var USERS: any[] = dados();

// following cards
export const FOLLOWING: IProduct[] = produtos();

// trending cards
export const TRENDING: ICategory[] = [
  {
    id: 1,
    name: 'horizontal'
  },
  {
    id: 2,
    name: 'horizontal'
  },
  {
    id: 3,
    name: 'teste'
  },
];

export const FeedBacks: ILista[] = [
  {
    id: 1,
    name: 'Janeiro'
  },
  {
    id: 2,
    name: 'Fevereiro'
  },
  {
    id: 3,
    name: 'Março'
  },
  {
    id: 4,
    name: 'Abril'
  },
  {
    id: 5,
    name: 'Maio'
  },
  {
    id: 6,
    name: 'Junho'
  },
];

export const LISTAS: ILista[] = [
  {
    id: 1,
    name: 'Janeiro'
  },
  {
    id: 2,
    name: 'Fevereiro'
  },
  {
    id: 3,
    name: 'Março'
  },
  {
    id: 4,
    name: 'Abril'
  },
  {
    id: 5,
    name: 'Maio'
  },
  {
    id: 6,
    name: 'Junho'
  },
];

// categories
export const CATEGORIES: ICategory[] = [
  { id: 1, name: 'Popular' },
  { id: 2, name: 'Newest' },
  { id: 3, name: 'Fashion' },
  { id: 4, name: 'Best deal' },
];

// article options
export const ARTICLE_OPTIONS: IArticleOptions[] = [
  {
    id: 1,
    title: 'Single room in center',
    description:
      'As Uber works through a huge amount of internal management turmoil, the company is also consolidating.',
    type: 'room',
    guests: 1,
    sleeping: { total: 1, type: 'sofa' },
    price: 89,
    user: USERS[0],
    image:
      'https://images.unsplash.com/photo-1543489822-c49534f3271f?fit=crop&w=450&q=80',
  },
  {
    id: 2,
    title: 'Cosy apartment',
    description:
      'Different people have different taste, and various types of music have many ways of leaving an impact on someone.',
    type: 'apartment',
    guests: 3,
    sleeping: { total: 2, type: 'bed' },
    price: 200,
    user: USERS[0],
    image:
      'https://images.unsplash.com/photo-1603034203013-d532350372c6?fit=crop&w=450&q=80',
  },
  {
    id: 3,
    title: 'Single room in center',
    description:
      'As Uber works through a huge amount of internal management turmoil, the company is also consolidating.',
    type: 'room',
    guests: 1,
    sleeping: { total: 1, type: 'sofa' },
    price: 89,
    user: USERS[0],
    image:
      'https://images.unsplash.com/photo-1543489822-c49534f3271f?fit=crop&w=450&q=80',
  },
];

// offers
export const OFFERS: IProduct[] = [
  {
    id: 1,
    type: 'vertical',
    title: 'FeedBacks',
    image:
      'https://images.unsplash.com/photo-1604998103924-89e012e5265a?fit=crop&w=450&q=80',
  },
  {
    id: 2,
    type: 'vertical',
    title: 'Férias',
    image:
      'https://images.unsplash.com/photo-1563492065599-3520f775eeed?fit=crop&w=450&q=80',
  },
  {
    id: 3,
    type: 'vertical',
    title: 'Get more followers and grow.',
    image:
      'https://images.unsplash.com/photo-1542744173-8e7e53415bb0?fit=crop&w=450&q=80',
  },
  {
    id: 4,
    type: 'vertical',
    title: 'New ways to meet your business goals.',
    image:
      'https://images.unsplash.com/photo-1497215728101-856f4ea42174?fit=crop&w=450&q=80',
  },
];

// rental locations
export const LOCATIONS: ILocation[] = [
  { id: 1, city: 'Paris', country: 'France' },
  { id: 2, city: 'Rome', country: 'Italy' },
  { id: 3, city: 'London', country: 'United Kingdom' },
];

// articles
export const ARTICLES: IArticle[] = [
  {
    id: 1,
    title: 'Flexible office space means growth.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay in the same location.',
    category: CATEGORIES[0],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1604998103924-89e012e5265a?fit=crop&w=450&q=80',
    user: USERS[0],
    timestamp: dayjs().unix(),
  },
  {
    id: 2,
    title: 'Global payments in a single integration.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay.',
    category: CATEGORIES[0],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1563492065599-3520f775eeed?fit=crop&w=450&q=80',
    user: USERS[1],
    timestamp: dayjs().unix(),
  },
  {
    id: 3,
    title: 'Working with the latest technologies.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay in the same location.',
    category: CATEGORIES[0],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1512470876302-972faa2aa9a4?fit=crop&w=450&q=80',
    user: USERS[2],
    timestamp: dayjs().unix(),
  },
  {
    id: 4,
    title: 'Office space means growth.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay in the same location.',
    category: CATEGORIES[0],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1497215728101-856f4ea42174?fit=crop&w=450&q=80',
    user: USERS[3],
    timestamp: dayjs().unix(),
  },
  {
    id: 5,
    title: 'Office space means growth.',
    description: `The mission of LinkedIn is simple: connect the world's professionals.`,
    category: CATEGORIES[1],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1596720426673-e4e14290f0cc?fit=crop&w=450&q=80',
    user: USERS[4],
    timestamp: dayjs().unix(),
  },
  {
    id: 6,
    title: 'Office space means growth.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay in the same location.',
    category: CATEGORIES[1],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1497215728101-856f4ea42174?fit=crop&w=450&q=80',
    user: USERS[5],
    timestamp: dayjs().unix(),
  },
  {
    id: 7,
    title: 'Office space means growth.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay in the same location.',
    category: CATEGORIES[1],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1497215728101-856f4ea42174?fit=crop&w=450&q=80',
    user: USERS[6],
    timestamp: dayjs().unix(),
  },
  {
    id: 8,
    title: 'Office space means growth.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay in the same location.',
    category: CATEGORIES[2],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1497215728101-856f4ea42174?fit=crop&w=450&q=80',
    user: USERS[1],
    timestamp: dayjs().unix(),
  },
  {
    id: 9,
    title: 'Office space means growth.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay in the same location.',
    category: CATEGORIES[2],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1497215728101-856f4ea42174?fit=crop&w=450&q=80',
    user: USERS[5],
    timestamp: dayjs().unix(),
  },
  {
    id: 10,
    title: 'Office space means growth.',
    description:
      'Rather than worrying about switching offices every couple years, you can instead stay in the same location.',
    category: CATEGORIES[2],
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1497215728101-856f4ea42174?fit=crop&w=450&q=80',
    user: USERS[6],
    timestamp: dayjs().unix(),
  },
  {
    id: 11,
    description:
      'A great to stay in Paris without feeling you are in the city!',
    category: CATEGORIES[3], // best deal
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1502602898657-3e91760cbb34?fit=crop&w=450&q=80',
    location: LOCATIONS[0],
    rating: 4.9,
    timestamp: dayjs().unix(),
  },
  {
    id: 12,
    description: 'Best Italy location in a bustling neighbourhood, 2 min.',
    category: CATEGORIES[3], // best deal
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1529154036614-a60975f5c760?fit=crop&w=450&q=80',
    location: LOCATIONS[1],
    rating: 4.5,
    timestamp: dayjs().unix(),
  },
  {
    id: 13,
    description:
      'The most beautiful and complex UI Kits built by VictorR.',
    category: CATEGORIES[3], // best deal
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1486299267070-83823f5448dd?fit=crop&w=450&q=80',
    location: LOCATIONS[2],
    rating: 4.8,
    timestamp: dayjs().unix(),
  },
];

// rental recommendations
export const RECOMMENDATIONS: IArticle[] = [
  {
    id: 1,
    description:
      'A great to stay in Paris without feeling you are in the city!',
    category: CATEGORIES[3], // best deal
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1502602898657-3e91760cbb34?fit=crop&w=450&q=80',
    location: LOCATIONS[0],
    rating: 4.9,
    offers: OFFERS,
    timestamp: dayjs().unix(),
  },
  {
    id: 2,
    description: 'Best Italy location in a bustling neighbourhood, 2 min.',
    category: CATEGORIES[3], // best deal
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1529154036614-a60975f5c760?fit=crop&w=450&q=80',
    location: LOCATIONS[1],
    rating: 4.5,
    offers: OFFERS,
    timestamp: dayjs().unix(),
  },
  {
    id: 3,
    description:
      'The most beautiful and complex UI Kits built by VictorR.',
    category: CATEGORIES[3], // best deal
    options: ARTICLE_OPTIONS,
    image:
      'https://images.unsplash.com/photo-1486299267070-83823f5448dd?fit=crop&w=450&q=80',
    location: LOCATIONS[2],
    rating: 4.8,
    offers: OFFERS,
    timestamp: dayjs().unix(),
  },
];

// chat messages
export const MESSSAGES = [
  {
    _id: 1,
    text: 'Bye, bye 👋🏻',
    createdAt: dayjs().subtract(1, 'm').toDate(),
    user: {
      _id: USERS[0].id,
      name: USERS[0].name,
      avatar: USERS[0].avatar,
    },
  },
  {
    _id: 2,
    text: 'Ok. Cool! See you 😁',
    createdAt: dayjs().subtract(2, 'm').toDate(),
    user: {
      _id: USERS[1].id,
      name: USERS[1].name,
      avatar: USERS[1].avatar,
    },
  },
  {
    _id: 3,
    text: 'Sure, just let me finish somerhing and I’ll call you.',
    createdAt: dayjs().subtract(3, 'm').toDate(),
    user: {
      _id: USERS[0].id,
      name: USERS[0].name,
      avatar: USERS[0].avatar,
    },
  },
  {
    _id: 4,
    text: 'Hey there! How are you today? Can we meet and talk about location? Thanks!',
    createdAt: dayjs().subtract(4, 'm').toDate(),
    user: {
      _id: USERS[1].id,
      name: USERS[1].name,
      avatar: USERS[1].avatar,
    },
  },
];

// extras cards
export const EXTRAS: IExtra[] = [
  {
    id: 1,
    name: 'BMW X5',
    time: dayjs().format('hh:00'),
    image: require('../assets/images/x5.png'),
    saved: false,
    booked: false,
    available: true,
  },
  {
    id: 2,
    name: 'Tesla',
    time: dayjs().format('hh:00'),
    image: require('../assets/images/tesla.png'),
    saved: false,
    booked: false,
    available: true,
  },
  {
    id: 3,
    name: 'Mercedes GLE',
    time: dayjs().format('hh:00'),
    image: require('../assets/images/gle.png'),
    saved: false,
    booked: false,
    available: false,
  },
];

// shopping basket recommentations
export const BASKET_RECOMMENDATIONS: IBasket['items'] = [
  {
    id: 4,
    title: 'Polo T-Shirt',
    description: 'Impeccably tailored in Italy.',
    image:
      'https://images.unsplash.com/photo-1586363104862-3a5e2ab60d99?fit=crop&w=450&q=80',
    stock: true,
    qty: 1,
    size: 'M',
    price: 200,
  },
  {
    id: 5,
    title: 'Orange Jacket',
    description: 'Mustard About Me - South Africa',
    image:
      'https://images.unsplash.com/photo-1599407950360-8b8642d423dc?fit=crop&w=450&q=80',
    stock: true,
    qty: 1,
    size: 'M',
    price: 489,
  },
];

// shopping basket
export const BASKET: IBasket = {
  subtotal: 750,
  recommendations: BASKET_RECOMMENDATIONS,
  items: [
    {
      id: 1,
      title: 'Leather Jacket',
      description: 'Impeccably tailored in Italy from lightweight navy.',
      image:
        'https://images.unsplash.com/photo-1562751361-ac86e0a245d1?fit=crop&w=450&q=80',
      stock: true,
      qty: 1,
      size: 'M',
      price: 250,
      qtys: [1, 2, 3, 4, 5],
      sizes: ['xs', 's', 'm', 'l', 'xl', 'xxl'],
    },
    {
      id: 2,
      title: 'Dark T-Shirt',
      description: 'Dark-grey slub wool, pintucked notch lapels.',
      image:
        'https://images.unsplash.com/photo-1521572163474-6864f9cf17ab?fit=crop&w=450&q=80',
      stock: true,
      qty: 1,
      size: 'l',
      price: 150,
      qtys: [1, 2, 3, 4, 5],
      sizes: ['xs', 's', 'm', 'l', 'xl', 'xxl'],
    },
    {
      id: 3,
      title: 'Leather Handbag',
      description: "Immaculate tailoring is TOM FORD's forte",
      image:
        'https://images.unsplash.com/photo-1608060434411-0c3fa9049e7b?fit=crop&w=450&q=80',
      stock: true,
      qty: 1,
      size: 'm',
      price: 350,
      qtys: [1, 2, 3],
      sizes: ['s', 'm', 'l'],
    },
  ],
};

// notifications
export const NOTIFICATIONS: INotification[] = [
  {
    id: 1,
    subject: 'New Message',
    message: 'Solicticação concluida',
    type: 'notification',
    business: true,
    read: false,
    createdAt: '2020/02/02',
  },
  {
    id: 2,
    subject: 'New Message',
    message: 'Solicitação incorreta',
    type: 'notification',
    business: true,
    read: false,
    createdAt: '2020/02/02',
  },
  {
    id: 3,
    subject: 'New Message',
    message: 'Solicitação incorreta.',
    type: 'notification',
    business: true,
    read: true,
    createdAt: '2020/02/02',
  },
  {
    id: 4,
    subject: 'New Message',
    message: 'Solicitação aceita.',
    type: 'notification',
    business: true,
    read: true,
    createdAt: '2020/02/02',
  },
  {
    id: 5,
    subject: 'New Message',
    message: 'Solicitação recusada.',
    type: 'notification',
    business: true,
    read: true,
    createdAt: '2020/02/02',
  },
];

export default {
  USERS,
  FOLLOWING,
  TRENDING,
  CATEGORIES,
  ARTICLES,
  RECOMMENDATIONS,
  MESSSAGES,
  EXTRAS,
  NOTIFICATIONS,
};
