import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Articles, Components, Home, Profile, Register, Pro, FeedBacks, Ferias, Ponto, Financeiro} from '../screens';
import {useScreenOptions, useTranslation} from '../hooks';
import { TextStyle } from 'react-native';
import { color } from 'react-native-reanimated';


const Stack = createStackNavigator();

export default () => {
  const {t} = useTranslation();
  const screenOptions = useScreenOptions();

  return (
    <Stack.Navigator screenOptions={screenOptions.stack}>
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerTransparent: true,headerShown:false}}
      />

      <Stack.Screen
        name="Home"
        component={Home}
        options={{title: t('navigation.home')}}
      />

      <Stack.Screen
        name="Components"
        component={Components}
        options={screenOptions.components}
      />

      <Stack.Screen
        name="Articles"
        component={Articles}
        options={{title: t('navigation.articles')}}
      />

      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="FeedBacks"
        component={FeedBacks}
        options={{headerShown: false}}
      />  
      
      <Stack.Screen
        name="Ponto"
        component={Ponto}
        options={{headerShown: false}}
      />  

      <Stack.Screen
        name="Financeiro"
        component={Financeiro}
        options={{headerShown: false}}
      />  

      <     Stack.Screen
        name="Ferias"
        component={Ferias}
        options={{headerShown: false}}
      />  
    </Stack.Navigator>
  );
};
