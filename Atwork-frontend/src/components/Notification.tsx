import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import { useNavigation } from '@react-navigation/core';

import Block from './Block';
import Image from './Image';
import Text from './Text';
import {INotification} from '../constants/types';
import {useTheme, useTranslation} from '../hooks';

const Notification = ({
  id, subject, message, read, business, createdAt, type}: INotification) => {
  const {t} = useTranslation();
  const {assets, colors, sizes} = useTheme();
  const navigation = useNavigation();


  return (
    <Block
      card
      flex={0}
      marginBottom={sizes.sm}>
      
      <Block
        paddingTop={sizes.sm}
        width={(sizes.width * 0.8)}
        justify="space-between">   
          <TouchableOpacity> 
          <Block marginBottom={sizes.s}>
          <Text
              p semibold
              color={colors.black}>
              {subject}
            </Text>
            <Text
              p
              color={colors.black}>
              {message}
            </Text>
          </Block>
        </TouchableOpacity>
        <Text color={colors.primary} marginLeft={sizes.s}>
          {read}
        </Text>
        <Text color={colors.primary} marginLeft={sizes.s}>
          {business}
        </Text>
        <Text color={colors.primary} marginLeft={sizes.s}>
          {type}
        </Text>
      </Block>
    </Block>
  );
};

export default Notification;
