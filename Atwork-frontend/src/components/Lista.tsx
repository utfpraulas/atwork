import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import { useNavigation } from '@react-navigation/core';

import Block from './Block';
import Image from './Image';
import Text from './Text';
import {ILista} from '../constants/types';
import {useTheme, useTranslation} from '../hooks';

const Lista = ({name, type}: ILista) => {
  const {t} = useTranslation();
  const {assets, colors, sizes} = useTheme();
  const navigation = useNavigation();

  const isHorizontal = type !== 'vertical';
  const CARD_WIDTH = (sizes.width - sizes.padding * 2 - sizes.sm) / 2;

  return (
    <Block
      card
      flex={0}
      row={isHorizontal}
      marginBottom={sizes.sm}
      width={isHorizontal ? CARD_WIDTH * 2 + sizes.sm : CARD_WIDTH}>
      
      <Block
        paddingTop={sizes.s}
        justify="space-between"
        paddingLeft={isHorizontal ? sizes.sm : 0}
        paddingBottom={isHorizontal ? sizes.s : 0}>   
          <TouchableOpacity> 
          <Block marginBottom={sizes.s}>
            <Text
              p
              color={colors.black}
              bold>
              {name}
            </Text>
          </Block>
        </TouchableOpacity>
        <Text color={colors.primary}>
          Crédito = 10h
        </Text>
        <Text color={colors.primary}>
          Débito = 2h
        </Text>
      </Block>
    </Block>
  );
};

export default Lista;
