import React, { useCallback } from 'react';
import { Platform, Linking, FlatList, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/core';

import { Block, Button, Image, Text } from '../components/';
import { useData, useTheme, useTranslation } from '../hooks/';
import { setuser } from '../constants/globals';
import * as dados from '../constants/mocks'

const isAndroid = Platform.OS === 'android';

const Profile = async () => {
  console.log("comeco");
  let user = await dados.userss();
  console.log("fim");
  const { t } = useTranslation();
  const navigation = useNavigation();
  const { assets, colors, sizes, gradients } = useTheme();


  const IMAGE_SIZE = (sizes.width - (sizes.padding + sizes.sm) * 2) / 3;
  const IMAGE_VERTICAL_SIZE =
    (sizes.width - (sizes.padding + sizes.sm) * 2) / 2;
  const IMAGE_MARGIN = (sizes.width - IMAGE_SIZE * 3 - sizes.padding * 2) / 2;
  const IMAGE_VERTICAL_MARGIN =
    (sizes.width - (IMAGE_VERTICAL_SIZE + sizes.sm) * 2) / 2;
  return (
    <Block safe marginTop={sizes.md}>
      <Block
        scroll
        paddingHorizontal={sizes.s}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: sizes.padding } }>
        <Block flex={0}>
          <Block
            padding={sizes.sm}
            paddingBottom={sizes.l}
            radius={sizes.cardRadius}
            color={colors.primary}>
            <Button
              row
              flex={0}
              justify="flex-start"
              onPress={() => navigation.goBack()}>
              <Image
                radius={0}
                width={10}
                height={18}
                color={colors.white}
                source={assets.arrow}
                transform={[{ rotate: '180deg' }]}
              />
              <Text p white marginLeft={sizes.s}>
                {t('profile.title')}
              </Text>
            </Button>
            <Block flex={0} align="center">
              <Image
                width={64}
                height={64}
                marginBottom={sizes.sm}
                source={{ uri: user?.avatar }}
              />
              <Text h5 center white>
                {user?.name}
              </Text>
              <Text p center white>
                {user?.department}
              </Text>
            </Block>
          </Block>

          <Block
            flex={0}
            radius={sizes.sm}
            shadow={!isAndroid} 
            marginTop={-sizes.l}
            marginHorizontal="8%"
            color="rgba(255,255,255,0.2)">
            <Block
              row
              blur
              flex={0}
              intensity={100}
              radius={sizes.sm}
              overflow="hidden"
              tint={colors.blurTint}
              justify="space-evenly"
              paddingVertical={sizes.sm}
              renderToHardwareTextureAndroid>
              <Block align="center">
                <Text h5>{}</Text>
                <Text>{t('profile.posts')}</Text>
              </Block>
              <Block align="center">
                <Text h5>{}</Text>
                <Text>{t('profile.followers')}</Text>
              </Block>
              <Block align="center">
                <Text h5>{}</Text>
                <Text>{t('profile.following')}</Text>
              </Block>
            </Block>
          </Block>

          {/* profile: about me */}
          <Block paddingHorizontal={sizes.sm}>
            <Text h5 semibold marginBottom={sizes.s} marginTop={sizes.sm}>
              {t('profile.aboutMe')}
            </Text>
            <Text p lineHeight={26}>
              {user?.about}
            </Text>
          </Block>

          {/* profile: photo album */}
          <Block paddingHorizontal={sizes.sm} marginTop={sizes.s}>
            <Block row align="center" justify="space-between">
              <Text h5 semibold>
                {t('common.album')}
              </Text>
              <Button>
                <Text p primary semibold>
                  {t('common.viewall')}
                </Text>
              </Button>
            </Block>

            {/* single card */}
            <Block>
              <Block card row>
                <Image
                  resizeMode="contain"
                  source={assets?.card1}
                  style={{ height: 114 }}
                />
                <Block padding={sizes.s} justify="space-between">
                  <Text p>Ferias</Text>
                  <TouchableOpacity>
                    <Block row align="center">
                      <Text p semibold marginRight={sizes.s} color={colors.link}>
                        Veja suas férias
                      </Text>
                      <Image source={assets.feriasv} color={colors.link} />
                    </Block>
                  </TouchableOpacity>
                </Block>
              </Block>
            </Block>
            {/* inline cards */}
            <Block row marginTop={sizes.sm}>
              <Block card marginRight={sizes.sm}>
                <Image
                  resizeMode="cover"
                  source={assets?.card2}
                  style={{ width: '100%' }}
                />
                <Block padding={sizes.s} justify="space-between">
                  <Text p marginBottom={sizes.s}>
                    Pagamentos
                  </Text>
                  <TouchableOpacity>
                    <Block row align="center">
                      <Text p semibold marginRight={sizes.s} color={colors.link}>
                        Veja suas folhas de PGTO
                      </Text>
                      <Image source={assets.arrow} color={colors.link} />
                    </Block>
                  </TouchableOpacity>
                </Block>
              </Block>
              <Block card>
                <Image
                  resizeMode="cover"
                  source={assets?.card3}
                  style={{ width: '100%' }}
                />
                <Block padding={sizes.s} justify="space-between">
                  <Text p marginBottom={sizes.s}>
                    Horarios
                  </Text>
                  <TouchableOpacity>
                    <Block row align="center">
                      <Text p semibold marginRight={sizes.s} color={colors.link}>
                        Veja seu relógio ponto
                      </Text>
                      <Image source={assets.arrow} color={colors.link} />
                    </Block>
                  </TouchableOpacity>
                </Block>
              </Block>
            </Block>
            {/* full image width card */}
            <Block card marginTop={sizes.sm}>
              <Image
                resizeMode="cover"
                source={assets?.card4}
                style={{ width: '100%' }}
              />
              <Text
                h5
                bold
                transform="uppercase"
                gradient={gradients.primary}
                marginTop={sizes.sm}>
                Trending
              </Text>
              <Text
                p
                marginTop={sizes.s}
                marginLeft={sizes.xs}
                marginBottom={sizes.sm}>
                The most beautiful and complex UI Kits built by VictorR.
              </Text>
              {/* user details */}
              <Block row marginLeft={sizes.xs} marginBottom={sizes.xs}>
                <Image
                  source={assets.avatar1}
                  style={{ width: sizes.xl, height: sizes.xl, borderRadius: sizes.s }}
                />
                <Block marginLeft={sizes.s}>
                  <Text p semibold>
                    Mathew Glock
                  </Text>
                  <Text p gray>
                    Posted on 28 February
                  </Text>
                </Block>
              </Block>
            </Block>
            {/* image background card */}
            <Block card padding={0} marginTop={sizes.sm}>
              <Image
                background
                resizeMode="cover"
                source={assets.card5}
                radius={sizes.cardRadius}>
                <Block color="rgba(0,0,0,0.3)" padding={sizes.padding}>
                  <Text h4 white marginBottom={sizes.sm}>
                    Flexible office space means growth.
                  </Text>
                  <Text p white>
                    Rather than worrying about switching offices every couple years,
                    you can instead stay in the same location.
                  </Text>
                  {/* user details */}
                  <Block row marginLeft={sizes.xs} marginTop={sizes.xxl}>
                    <Image
                      source={assets.avatar2}
                      style={{
                        width: sizes.xl,
                        height: sizes.xl,
                        borderRadius: sizes.s,
                      }}
                    />
                    <Block marginLeft={sizes.s}>
                      <Text p white semibold>
                        Gabriela Carvalho
                      </Text>
                      <Text p white>
                        Marketing Manager
                      </Text>
                    </Block>
                  </Block>
                </Block>
              </Image>
            </Block>
          </Block>
        </Block>
      </Block>
    </Block>
  );
};

export default Profile;
