import React, { useCallback, useState } from 'react';
import { Platform, Linking, FlatList, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { SafeAreaView, StyleSheet, TextInput } from "react-native";
import { useNavigation } from '@react-navigation/core';
import axios from 'axios';

import { Block, Button, Image, Text, Input, Modal} from '../components/';
import { useData, useTheme, useTranslation } from '../hooks/';

const isAndroid = Platform.OS === 'android'; 

const Profile = () => {
  const { user } = useData();
  const { t } = useTranslation();
  const navigation = useNavigation();
  const { assets, colors, sizes, gradients } = useTheme();
  const [number, onChangeNumber] = React.useState(null);
  interface IPonto {
    dia: string;
    horario: string;
    motivo: string;
    userid: number;
  }
  var [ponto, setPonto] = useState<IPonto>({
    dia: '',
    horario: '',
    motivo: '',
    userid: 0
  });

  const handleChange = useCallback(
    (value) => {
      setPonto((state) => ({ ...state, ...value }));
    },
    [setPonto],
  );

  const handleCreate = useCallback(() => {
    axios.post('http://192.168.0.101:3001/nota10/ponto/create', {
      body: {
        dia: ponto.dia,
        horario: ponto.horario,
        motivo: ponto.motivo,
        userid: user.id
      }
    })
      .then(response => {
        console.log('handleSignUp', ponto);
        if (response.data){
          console.log('Criado com sucesso');
        } else {
          console.log('erros');
          
        }
      });
  }, [ponto]);

  const IMAGE_SIZE = (sizes.width - (sizes.padding + sizes.sm) * 2) / 3;
  const IMAGE_VERTICAL_SIZE =
    (sizes.width - (sizes.padding + sizes.sm) * 2) / 2;
  const IMAGE_MARGIN = (sizes.width - IMAGE_SIZE * 3 - sizes.padding * 2) / 2;
  const IMAGE_VERTICAL_MARGIN =
    (sizes.width - (IMAGE_VERTICAL_SIZE + sizes.sm) * 2) / 2;
  return (
    <Block safe marginTop={sizes.md}>
      <Block
        scroll
        paddingHorizontal={sizes.s}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: sizes.padding }}>
        <Block flex={0}>
          <Block
            padding={sizes.sm}
            paddingBottom={sizes.l}
            radius={sizes.cardRadius}
            color={colors.primary}
            >
            <Button
              row
              flex={0}
              justify="flex-start"
              onPress={() => navigation.goBack()}>
              <Image
                radius={0}
                width={10}
                height={18}
                color={colors.white}
                source={assets.arrow}
                transform={[{ rotate: '180deg' }]}
              />
              <Text p white marginLeft={sizes.s}>
                {'Acerto de Ponto'}
              </Text>
            </Button>
            <Block flex={0} align="center">
              <Text h5 center white>
                Colaborador: {user?.name}
              </Text>
              <Text p center white>
                {user?.department}
              </Text>
            </Block>
          </Block>


          <Block
            flex={0}
            radius={sizes.sm}
            shadow={!isAndroid} 
            marginTop={sizes.sm}
            marginHorizontal="8%"
            color="rgba(255,255,255,0.2)">
            <Block align="center">
              <Text h5></Text>
              <Text bold> Acerto de Ponto dia: </Text>
            </Block>
            <Input
                marginBottom={sizes.m}
                placeholder={'Data'}
                success={Boolean(ponto.dia)}
                onChangeText={(value) => handleChange({ dia: value })}
              />
            <Block align="center">
              <Text h5></Text>
              <Text bold> Horario de Marcação: </Text>
            </Block>
            <Input
                marginBottom={sizes.m}
                placeholder={'Horário'}
                success={Boolean(ponto.horario)}
                onChangeText={(value) => handleChange({ horario: value })}
              />
            <Block align="center">
              <Text h5></Text>
              <Text bold> Motivo: </Text>
            </Block>
            <Input
                marginBottom={sizes.m}
                placeholder={'Texto'}
                success={Boolean(ponto.motivo)}
                onChangeText={(value) => handleChange({ motivo: value })}
              />
              <Button gradient={gradients.primary}
                onPress={handleCreate}
                marginVertical={sizes.s}
                marginHorizontal={sizes.sm * 4}>
                <Text semibold black transform="uppercase">
                  Enviar
                </Text>
                
              </Button>
          </Block>   
        </Block>
      </Block>
    </Block>
  );
};

export default Profile;
