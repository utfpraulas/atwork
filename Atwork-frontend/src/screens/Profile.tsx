import React, { useCallback } from 'react';
import { Platform, Linking, FlatList, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/core';

import { Block, Button, Image, Text } from '../components/';
import { useData, useTheme, useTranslation } from '../hooks/';
import axios from 'axios';
import * as global from '../constants/globals'
import * as dados from '../constants/mocks'

const isAndroid = Platform.OS === 'android';

export default function mds() {
  console.log("comeco");
  var user = dados.userss()
  /*const handleSignUp = useCallback(() => {
    axios.post(`http://192.168.0.101:3001/nota10/user/profile/${global.userid}`)
      .then(response => {
        console.log('veio');
        user = response.data
      });
  }, [user]);
  console.log("fim");*/
  //const { user } = useData();
  const { t } = useTranslation();
  const navigation = useNavigation();
  const { assets, colors, sizes, gradients } = useTheme();
  async function carregavalores() {
    console.log('valorez');

  }
  const IMAGE_SIZE = (sizes.width - (sizes.padding + sizes.sm) * 2) / 3;
  const IMAGE_VERTICAL_SIZE =
    (sizes.width - (sizes.padding + sizes.sm) * 2) / 2;
  const IMAGE_MARGIN = (sizes.width - IMAGE_SIZE * 3 - sizes.padding * 2) / 2;
  const IMAGE_VERTICAL_MARGIN =
    (sizes.width - (IMAGE_VERTICAL_SIZE + sizes.sm) * 2) / 2;
    return (
      <Block safe marginTop={sizes.md}>
        <Block
          scroll
          paddingHorizontal={sizes.s}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: sizes.padding }}>
          <Block flex={0}>
            <Block
              padding={sizes.sm}
              paddingBottom={sizes.l}
              radius={sizes.cardRadius}
              color={colors.primary}>
              <Button
                row
                flex={0}
                justify="flex-start"
                onPress={() => navigation.goBack()}>
                <Image
                  radius={0}
                  width={10}
                  height={18}
                  color={colors.white}
                  source={assets.arrow}
                  transform={[{ rotate: '180deg' }]}
                />
                <Text p white marginLeft={sizes.s}>
                  {t('profile.title')}
                </Text>
              </Button>
              <Block flex={0} align="center">
                <Image
                  width={64}
                  height={64}
                  marginBottom={sizes.sm}
                  source={{ uri: user?.avatar }}
                />
                <Text h5 center white>
                  {user?.name}
                </Text>
                <Text p center white>
                  {user?.department}
                </Text>
              </Block>
            </Block>

            <Block
              flex={0}
              radius={sizes.sm}
              shadow={!isAndroid}
              marginTop={-sizes.l}
              marginHorizontal="8%"
              color="rgba(255,255,255,0.2)">
              <Block
                row
                blur
                flex={0}
                intensity={100}
                radius={sizes.sm}
                overflow="hidden"
                tint={colors.blurTint}
                justify="space-evenly"
                paddingVertical={sizes.sm}
                renderToHardwareTextureAndroid>
                <Block align="center">
                  <Text h5>{10}</Text>
                  <Text>{t('profile.posts')}</Text>
                </Block>
                <Block align="center">
                  <Text h5>{15}</Text>
                  <Text>{t('profile.followers')}</Text>
                </Block>
                <Block align="center">
                  <Text h5>{0}</Text>
                  <Text>{t('profile.following')}</Text>
                </Block>
              </Block>
            </Block>


            <Block paddingHorizontal={sizes.sm}>
              <Text h5 semibold marginBottom={sizes.s} marginTop={sizes.sm}>
                {t('profile.aboutMe')}
              </Text>
              <Text p lineHeight={26}>
                {user?.about}
              </Text>
            </Block>


            <Block paddingHorizontal={sizes.sm} marginTop={sizes.s}>
              <Block row align="center" justify="space-between">
                <Text h5 semibold>
                  {t('common.album')}
                </Text>
                <Button>
                  <Text p primary semibold>
                    {t('common.viewall')}
                  </Text>
                </Button>
              </Block>


              <Block>
                <Block card row>
                  <Block padding={sizes.s} justify="space-between">
                    <Text p>Ferias</Text>
                    <TouchableOpacity>
                      <Block row align="center">
                        <Text p semibold marginRight={sizes.s} color={colors.link}>
                          Veja suas férias
                        </Text>

                      </Block>
                    </TouchableOpacity>
                  </Block>
                </Block>
              </Block>

              <Block row marginTop={sizes.sm}>
                <Block card marginRight={sizes.sm}>
                  <Block padding={sizes.s} justify="space-between">
                    <Text p marginBottom={sizes.s}>
                      Pagamentos
                    </Text>
                    <TouchableOpacity>
                      <Block row align="center">
                        <Text p semibold marginRight={sizes.s} color={colors.link}>
                          Veja suas folhas de PGTO
                        </Text>
                      </Block>
                    </TouchableOpacity>
                  </Block>
                </Block>
                <Block card>
                  <Block padding={sizes.s} justify="space-between">
                    <Text p marginBottom={sizes.s}>
                      Horarios
                    </Text>
                    <TouchableOpacity>
                      <Block row align="center">
                        <Text p semibold marginRight={sizes.s} color={colors.link}>
                          Veja seu relógio ponto
                        </Text>
                      </Block>
                    </TouchableOpacity>
                  </Block>
                </Block>
              </Block>
            </Block>
          </Block>
        </Block>
      </Block>
    );
  };

