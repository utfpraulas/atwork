import React, { useCallback } from 'react';
import { Platform, Linking, FlatList, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/core';

import { Block, Button, Image, Text } from '../components/';
import { useData, useTheme, useTranslation } from '../hooks/';

const isAndroid = Platform.OS === 'android';

const Profile = () => {
  const { user } = useData();
  const { t } = useTranslation();
  const navigation = useNavigation();
  const { assets, colors, sizes, gradients } = useTheme();

  const IMAGE_SIZE = (sizes.width - (sizes.padding + sizes.sm) * 2) / 3;
  const IMAGE_VERTICAL_SIZE =
    (sizes.width - (sizes.padding + sizes.sm) * 2) / 2;
  const IMAGE_MARGIN = (sizes.width - IMAGE_SIZE * 3 - sizes.padding * 2) / 2;
  const IMAGE_VERTICAL_MARGIN =
    (sizes.width - (IMAGE_VERTICAL_SIZE + sizes.sm) * 2) / 2;
  return (
    <Block safe marginTop={sizes.md}>
      <Block
        scroll
        paddingHorizontal={sizes.s}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: sizes.padding }}>
        <Block flex={0}>
          <Block
            padding={sizes.sm}
            paddingBottom={sizes.l}
            radius={sizes.cardRadius}
            color={colors.primary}>
            <Button
              row
              flex={0}
              justify="flex-start"
              onPress={() => navigation.goBack()}>
              <Image
                radius={0}
                width={10}
                height={18}
                color={colors.white}
                source={assets.arrow}
                transform={[{ rotate: '180deg' }]}
              />
              <Text p white marginLeft={sizes.s}>
                {'Folha de Pagamento'}
              </Text>
            </Button>
            <Block flex={0} align="center">
              <Text h5 center white>
                Colaborador: {user?.name}
              </Text>
              <Text p center white>
                {user?.department}
              </Text>
            </Block>
          </Block>

          {/* profile: stats */}
          <Block
            flex={0}
            radius={sizes.sm}
            shadow={!isAndroid} // disabled shadow on Android due to blur overlay + elevation issue
            marginTop={-sizes.l}
            marginHorizontal="8%"
            color="rgba(255,255,255,0.2)">
            <Block
              row
              blur
              flex={0}
              intensity={100}
              radius={sizes.sm}
              overflow="hidden"
              tint={colors.blurTint}
              justify="space-evenly"
              paddingVertical={sizes.sm}
              renderToHardwareTextureAndroid>
              <Block align="center">
                <Text h5></Text>
                <Text> Recibos: </Text>
              </Block>
              <Block align="center">
                <Text h5></Text>
                <Text>01/2022</Text>
              </Block>
            </Block>
          </Block>

          <Block paddingHorizontal={sizes.sm} marginTop={sizes.s}>
            <Block row align="center" justify="space-between">
              <Text h5 semibold>
                Recibo
              </Text>
              <Text p primary semibold>
                Desempenho Total 5
              </Text>
            </Block>
            <Block card marginTop={sizes.sm}>
              <Text
                transform="uppercase"
                gradient={gradients.primary}
                marginTop={sizes.sm}>
                Razão Social:
              </Text>
              <Text
                transform="uppercase"
                gradient={gradients.primary}
                marginTop={sizes.sm}>
                CNPJ:
              </Text>
              <Text
                transform="uppercase"
                gradient={gradients.primary}
                marginTop={sizes.sm}>
                Estabelecimento:
              </Text>
            </Block>
            <Block card marginTop={sizes.sm}>
            <Text semibold>Folha de Pagamento:</Text>
            </Block>
          </Block>
        </Block>
      </Block>
    </Block>
  );
};

export default Profile;
