import React, { useCallback } from 'react';
import { Platform, Linking, FlatList, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/core';

import { Block, Button, Image, Text, Input} from '../components/';
import { useData, useTheme, useTranslation } from '../hooks/';
import * as dados from '../constants/mocks'

const isAndroid = Platform.OS === 'android';

const Profile = () => {
  let user = dados.userss();
  const { t } = useTranslation();
  const navigation = useNavigation();
  const { assets, colors, sizes, gradients } = useTheme();

  const IMAGE_SIZE = (sizes.width - (sizes.padding + sizes.sm) * 2) / 3;
  const IMAGE_VERTICAL_SIZE =
    (sizes.width - (sizes.padding + sizes.sm) * 2) / 2;
  const IMAGE_MARGIN = (sizes.width - IMAGE_SIZE * 3 - sizes.padding * 2) / 2;
  const IMAGE_VERTICAL_MARGIN =
    (sizes.width - (IMAGE_VERTICAL_SIZE + sizes.sm) * 2) / 2;
  return (
    <Block safe marginTop={sizes.md}>
      <Block
        scroll
        paddingHorizontal={sizes.s}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: sizes.padding }}>
        <Block flex={0}>
          <Block
            padding={sizes.sm}
            paddingBottom={sizes.l}
            radius={sizes.cardRadius}
            color={colors.primary}>
            <Button
              row
              flex={0}
              justify="flex-start"
              onPress={() => navigation.goBack()}>
              <Image
                radius={0}
                width={10}
                height={18}
                color={colors.white}
                source={assets.arrow}
                transform={[{ rotate: '180deg' }]}
              />
              <Text p white marginLeft={sizes.s}>
                {'Avaliações'}
              </Text>
            </Button>
            <Block flex={0} align="center">
              <Text h5 center white>
                Colaborador: {user?.name}
              </Text>
              <Text p center white>
                {user?.department}
              </Text>
            </Block>
          </Block>

          {/* profile: stats */}
          <Block
            flex={0}
            radius={sizes.sm}
            shadow={!isAndroid} // disabled shadow on Android due to blur overlay + elevation issue
            marginTop={-sizes.l}
            marginHorizontal="8%"
            color="rgba(255,255,255,0.2)">
            <Block
              row
              blur
              flex={0}
              intensity={100}
              radius={sizes.sm}
              overflow="hidden"
              tint={colors.blurTint}
              justify="space-evenly"
              paddingVertical={sizes.sm}
              renderToHardwareTextureAndroid>
              <Block align="center">
                <Text h5></Text>
                <Text semibold> Periodos: </Text>
              </Block>
              <Block align="center">
                <Text h5></Text>
                <Text semibold>01/2022</Text>
              </Block>
            </Block>
          </Block>

          {/* profile: about me */}
          <Block paddingHorizontal={sizes.sm}>
            <Text p h5 semibold marginBottom={sizes.s} marginTop={sizes.sm}>
              Avaliação do Periodo de 01/2022 a 06/2022
            </Text>
            <Text marginBottom={sizes.s} marginTop={sizes.sm}>
              {user?.funcao}
            </Text>
            <Text marginBottom={sizes.s} marginTop={sizes.sm}>
              Tempo de Empresa: 2 anos / Setor: Desenvolvimento
            </Text>
          </Block>

          <Block paddingHorizontal={sizes.sm} marginTop={sizes.s}>
            <Block row align="center" justify="space-between">
              <Text h5 semibold>
                Avaliação
              </Text>
              <Text p primary semibold>
                Desempenho Total 5 
              </Text>
            </Block>
            <Block card marginTop={sizes.sm}>
              <Text
                transform="uppercase"
                gradient={gradients.primary}
                marginTop={sizes.sm}>
                Item                      Descrição                     Valor
              </Text>
            </Block>
            <Block card marginTop={sizes.sm}>
              <Text
                p
                marginTop={sizes.s}
                marginLeft={sizes.xs}
                marginBottom={sizes.sm}>
                01                      Experiencia                     5
              </Text>
            </Block>
            <Block card marginTop={sizes.sm}>
              <Text
                p
                marginTop={sizes.s}
                marginLeft={sizes.xs}
                marginBottom={sizes.sm}>
                01                      Conduta                         4
              </Text>
            </Block>
          </Block>
        </Block>
      </Block>
    </Block>
  );
};

export default Profile;
