import React, { useCallback, useState } from 'react';
import { useNavigation } from '@react-navigation/core';

import { Block, Text, Notification } from '../components/';
import { useData, useTheme, useTranslation } from '../hooks/';
import { NOTIFICATIONS } from '../constants/mocks';


const Notificacao = () => {

  const { notifications } = useData();
  const [notificacoes, setNotification] = useState(notifications)
  const { user } = useData();
  const { t } = useTranslation();
  const navigation = useNavigation();
  const { assets, colors, sizes, gradients } = useTheme();

  const handleProducts = useCallback(
    () => {
    setNotification(notifications)
    },
    [notifications, setNotification],
  );
  //setNotification(notifications)
  return (
    <Block safe marginTop={sizes.md}>
      <Block
        scroll
        paddingHorizontal={sizes.s}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: sizes.padding }}>
        <Block flex={0}>
          <Block
            padding={2}
            paddingBottom={5}
            radius={sizes.cardRadius}
            color={colors.primary}>
            <Block
              margin={sizes.m}
              flex={0} align="center">
              <Text h5 center white>
                {user?.name}
              </Text>
            </Block>
          </Block>
          <Block
            scroll
            paddingHorizontal={sizes.padding}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: sizes.l }}>
            <Block row wrap="wrap" justify="space-between" marginTop={sizes.sm}>
              {notifications?.map((note) => (
                <Notification {...note} key={`card-${note?.id}`} />
              ))}
            </Block>
          </Block>
        </Block>
      </Block>
    </Block>
  );
};

export default Notificacao;
